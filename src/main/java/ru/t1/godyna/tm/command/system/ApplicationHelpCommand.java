package ru.t1.godyna.tm.command.system;

import ru.t1.godyna.tm.api.model.ICommand;
import ru.t1.godyna.tm.command.AbstractCommand;

import java.util.Collection;

public final class ApplicationHelpCommand extends AbstractSystemCommand {

    private final String NAME = "help";

    private final String ARGUMENT = "-h";

    private final String DESCRIPTION = "Show list arguments.";

    @Override
    public void execute() {
        System.out.println("[HELP]");
        final Collection<AbstractCommand> commands = getCommandService().getCommands();
        for (final ICommand command : commands) System.out.println(command.toString());
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
