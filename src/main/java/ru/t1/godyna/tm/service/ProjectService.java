package ru.t1.godyna.tm.service;

import ru.t1.godyna.tm.api.repository.IProjectRepository;
import ru.t1.godyna.tm.api.service.IProjectService;
import ru.t1.godyna.tm.enumerated.Status;
import ru.t1.godyna.tm.exception.entity.ProjectNotFoundException;
import ru.t1.godyna.tm.exception.entity.StatusEmptyException;
import ru.t1.godyna.tm.exception.field.*;
import ru.t1.godyna.tm.model.Project;

public final class ProjectService extends AbstractUserOwnedService<Project, IProjectRepository> implements IProjectService {

    public ProjectService(final IProjectRepository repository) {
        super(repository);
    }

    @Override
    public Project create(final String userId, final String name, final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        if (description == null)
            throw new DescriptionEmptyException();
        final Project project = new Project();
        project.setName(name);
        project.setDescription(description);
        add(userId, project);
        return project;
    }

    @Override
    public Project create(final String userId, final String name) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = new Project();
        project.setName(name);
        add(userId, project);
        return project;
    }

    @Override
    public Project updateById(final String userId, final String id, final String name, final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = findOneById(userId, id);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project updateByIndex(final String userId, final Integer index, final String name, final String description) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (name == null || name.isEmpty())
            throw new NameEmptyException();
        final Project project = findOneByIndex(userId, index);
        project.setName(name);
        project.setDescription(description);
        return project;
    }

    @Override
    public Project changeProjectStatusByIndex(final String userId, final Integer index, final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (index == null || index < 0 || index >= repository.getSize())
            throw new IndexIncorrectException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneByIndex(userId, index);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

    @Override
    public Project changeProjectStatusById(final String userId, final String id, final Status status) {
        if (userId == null || userId.isEmpty())
            throw new UserIdEmptyException();
        if (id == null || id.isEmpty())
            throw new ProjectIdEmptyException();
        if (status == null)
            throw new StatusEmptyException();
        final Project project = findOneById(userId, id);
        if (project == null)
            throw new ProjectNotFoundException();
        project.setStatus(status);
        return project;
    }

}
