package ru.t1.godyna.tm.command.system;

public final class ApplicationAboutCommand extends AbstractSystemCommand {

    private final String NAME = "about";

    private final String ARGUMENT = "-a";

    private final String DESCRIPTION = "Shows developer info";

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("name: Alexey Godyna");
        System.out.println("E-mail: agodyna@t1-consulting.ru");
        System.out.println("E-mail: algo2791@yandex.ru");
    }

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
