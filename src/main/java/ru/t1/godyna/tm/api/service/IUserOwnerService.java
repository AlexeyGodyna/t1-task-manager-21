package ru.t1.godyna.tm.api.service;

import ru.t1.godyna.tm.api.repository.IUserOwnedRepository;
import ru.t1.godyna.tm.enumerated.Sort;
import ru.t1.godyna.tm.model.AbstractUserOwnedModel;

import java.util.List;

public interface IUserOwnerService<M extends AbstractUserOwnedModel> extends IUserOwnedRepository<M>, IService<M> {

    List<M> findAll(String userId, Sort sort);

}
