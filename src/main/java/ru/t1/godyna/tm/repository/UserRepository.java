package ru.t1.godyna.tm.repository;

import ru.t1.godyna.tm.api.repository.IUserRepository;
import ru.t1.godyna.tm.model.User;

public class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public User findByLogin(final String login) {
        return records
                .stream()
                .filter(m -> login.equals(m.getLogin()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public User findByEmail(final String email) {
        return records
                .stream()
                .filter(m -> email.equals(m.getEmail()))
                .findFirst()
                .orElse(null);
    }

    @Override
    public Boolean isLoginExist(final String login) {
        return records
                .stream()
                .anyMatch(m -> login.equals(m.getLogin()));
    }

    @Override
    public Boolean isMailExist(final String email) {
        return records
                .stream()
                .anyMatch(m -> email.equals(m.getEmail()));
    }

}
